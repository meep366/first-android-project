package net.jackcroft.androidtest;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;


public class MainActivity extends Activity {
	private static String GAME_TYPE="GAME_TYPE";
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void oblivion(View v) {
		Intent intent = new Intent(this, IngredientOptionsActivity.class);
		intent.putExtra(GAME_TYPE, "OBLIVION");
		startActivity(intent);
	}
	
	public void skyrim(View v) {
		Intent intent = new Intent(this, IngredientOptionsActivity.class);
		intent.putExtra(GAME_TYPE, "SKYRIM");
		startActivity(intent);
	}
}
