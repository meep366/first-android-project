package net.jackcroft.androidtest;
import java.util.ArrayList;
/**
 * @author Jack
 *
 */
public class Ingredient implements Comparable<Ingredient>{
	
	private final String name;
	private final ArrayList<String> effects;
	
	public Ingredient(String n, String e1, String e2, String e3, String e4)
	{
		effects=new ArrayList<String>();
		name=n;
		effects.add(e1);
		effects.add(e2);
		effects.add(e3);
		effects.add(e4);
	}
	
	public Ingredient(String n, ArrayList<String> e)
	{
		name=n;
		effects=e;
	}
	
	public Ingredient(Ingredient ing)
	{
		effects=new ArrayList<String>();
		name=ing.getName();
		for(int i=0;i<ing.getEffects().size();i++)
			effects.add(ing.getEffect(i));
	}
	
	public ArrayList<String> getEffects()
	{
		return effects;
	}
	
	public String getEffect(int i)
	{
		return effects.get(i);
	}
	
	public String getName()
	{
		return name;
	}
	
	public boolean hasEffect(String effect)
	{
		for(int i=0;i<effects.size();i++)
		{
			if(effects.get(i).equalsIgnoreCase(effect))
				return true;
		}
		return false;
	}
	
	public boolean equals(Object value)
	{
		if(value instanceof Ingredient)
		{
			return ((Ingredient) value).getName().equalsIgnoreCase(this.getName());
		}
		return false;
	}
	
	public String toString()
	{
		return name+": "+effects.get(0)+", "+effects.get(1)+", "+effects.get(2)+", "+effects.get(3);
	}

	public int compareTo(Ingredient i) 
	{
		return this.getName().compareToIgnoreCase(i.getName());
		
	}
}