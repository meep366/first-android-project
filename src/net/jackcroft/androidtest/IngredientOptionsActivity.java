package net.jackcroft.androidtest;

import java.io.File;
import java.io.FileNotFoundException;
import net.jackcroft.androidtest.util.SystemUiHider;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.app.FragmentTransaction;
import android.content.Context;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class IngredientOptionsActivity extends Activity {

	private GameType gameType;
	private IngredientFileStore fileStore;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ingredient_options);
		
		 final ActionBar actionBar = getActionBar();
		    

		    // Specify that tabs should be displayed in the action bar.
		    actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		    // Create a tab listener that is called when the user changes tabs.
		    ActionBar.TabListener tabListener = new ActionBar.TabListener() {
		        public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
		            // show the given tab
		        }

		        public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
		            // hide the given tab
		        }

		        public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
		            // probably ignore this event
		        }
		    };

		    // Add 3 tabs, specifying the tab's text and TabListener
		    for (int i = 0; i < 2; i++) {
		        actionBar.addTab(
		                actionBar.newTab()
		                        .setText("Tab " + (i + 1))
		                        .setTabListener(tabListener));
		    }
		    
		    String type=getIntent().getStringExtra("GAME_TYPE");
		    gameType=GameType.valueOf(type);

		    try {
		    	String dir=getFilesDir().getPath().toString();
		    	if(gameType==GameType.OBLIVION) {
		    		File currentFile=new File(dir+"/currentoblivioningredients");
		    		if(!currentFile.exists()) {
		    			openFileOutput(dir+"/currentoblivioningredients",Context.MODE_PRIVATE);
		    		}
		    		fileStore=new IngredientFileStore(gameType, getResources().openRawResource(R.raw.oblivioningredients),currentFile);

		    	}
		    	else if(gameType==GameType.SKYRIM) {
		    		File currentFile=new File(dir+"/currentskyrimingredients");
		    		if(!currentFile.exists()) {
		    			openFileOutput(dir+"/currentskyrimingredients",Context.MODE_PRIVATE);
		    		}
		    		fileStore=new IngredientFileStore(gameType, getResources().openRawResource(R.raw.skyrimingredients),currentFile);

		    	}
		    }
		    catch (FileNotFoundException e) {
		    	e.printStackTrace();
		    }
	}
	
	public void add(View v) {
		EditText text=(EditText)findViewById(R.id.ingredientText);
		fileStore.addCurrentIngredient(text.getText().toString());
	}
	
	public void delete(View v) {
		EditText text=(EditText)findViewById(R.id.ingredientText);
		fileStore.deleteIngredient(text.getText().toString());
	}
}
