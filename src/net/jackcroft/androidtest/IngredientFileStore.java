package net.jackcroft.androidtest;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/**
 * @author Jack
 *
 */
public class IngredientFileStore {
	
	private ArrayList<Ingredient> ingredients=new ArrayList<Ingredient>();
	private ArrayList<Ingredient> currentIngredients=new ArrayList<Ingredient>();
	private IngredientList list;
	private File currentFile;
	
	public IngredientFileStore(GameType gameType, InputStream input, File current) {
	
		currentFile=current;
		ingredients=loadFiles(input);
		list=new IngredientList(ingredients);
		readCurrentIngredients();
	}
	
	private ArrayList<Ingredient> loadFiles(InputStream input) {
		String name="";
		String effect1="";
		String effect2="";
		String effect3="";
		String effect4="";
		ArrayList<Ingredient> ingredientsFromFile=new ArrayList<Ingredient>();
		Scanner reader=new Scanner(input);
		int currentLine=0;
		while(reader.hasNextLine())	{
			if(currentLine==0) {
				name=reader.nextLine();
				currentLine++;
			}
			else if(currentLine==1)	{
				effect1=reader.nextLine();
				currentLine++;
			}
			else if(currentLine==2)	{
				effect2=reader.nextLine();
				currentLine++;
			}
			else if(currentLine==3)	{
				effect3=reader.nextLine();
				currentLine++;
			}
			else if(currentLine==4)	{
				effect4=reader.nextLine();
				Ingredient i=new Ingredient(name,effect1,effect2,effect3,effect4);
				ingredientsFromFile.add(i);
				currentLine=0;
			}
		}
		reader.close();
		return ingredientsFromFile;
	}
	
	public ArrayList<Ingredient> getIngredients() {
		return ingredients;
	}
	
	public ArrayList<Ingredient> getCurrentIngredients() {
		return currentIngredients;
	}
	
	public void printToFile(String ingredientName) {
		try {
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(currentFile, true)));
			writer.println(ingredientName);
			writer.close();
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addCurrentIngredient(String ingredient) {
		ingredient=ingredient.toLowerCase();
		readCurrentIngredients();
		if(currentIngredients.contains(list.getIngredient(ingredient)))
			return;

		currentFile.delete();
		currentIngredients.add(list.getIngredient(ingredient));
		Collections.sort(currentIngredients);
		for(int i=0;i<currentIngredients.size();i++) {
			printToFile(currentIngredients.get(i).getName());
		}
		readCurrentIngredients();

	}
	
	public void readCurrentIngredients() {
		try	{
			Scanner reader=new Scanner(currentFile);
			while(reader.hasNextLine())	{
				Ingredient i=new Ingredient(list.getIngredient(reader.nextLine()));
				if(!currentIngredients.contains(i))
					currentIngredients.add(i);
			}
			reader.close();
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}

	public boolean deleteIngredient(String name) {
		name=name.toLowerCase();
		Ingredient ingredient=list.getIngredient(name);
		readCurrentIngredients();
		if(!currentIngredients.contains(ingredient))
			return false;
		
		currentFile.delete();
		currentIngredients.remove(ingredient);
		Collections.sort(currentIngredients);
		for(int i=0;i<currentIngredients.size();i++)
			printToFile(currentIngredients.get(i).getName());
		
		readCurrentIngredients();
		return true;
	}
}
