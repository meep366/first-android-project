package net.jackcroft.androidtest;
import java.util.ArrayList;
import java.util.HashMap;

import net.jackcroft.androidtest.Ingredient;

/**
 * @author Jack
 *
 */
public class IngredientList {
	private ArrayList<Ingredient> ingredients=new ArrayList<Ingredient>();
	private ArrayList<String> effects=new ArrayList<String>();
	
	public IngredientList(ArrayList<Ingredient> list)
	{
		ingredients=list;
		for(int i=0;i<ingredients.size();i++)
		{
			for(int j=0;j<ingredients.get(i).getEffects().size();j++)
			{
				if(!effects.contains(ingredients.get(i).getEffect(j)))
					effects.add(ingredients.get(i).getEffect(j));
			}
		}
	}
	
	public boolean isValidIngredient(String ingredient)
	{
		for(int i=0;i<ingredients.size();i++)
		{
			if(ingredients.get(i).getName().equalsIgnoreCase(ingredient))
			{
				return true;
			}
		}
		return false;
	}
	
	public Ingredient getIngredient(String name)
	{
		for(int i=0;i<ingredients.size();i++)
		{
			if(ingredients.get(i).getName().equalsIgnoreCase(name))
				return ingredients.get(i);
		}
		return null;
	}
	
	public ArrayList<String> sharedEffects(Ingredient one, Ingredient two, int numEffects)
	{
		ArrayList<String> sharedEffects=new ArrayList<String>();
		for(int i=0;i<numEffects;i++)
		{
			for(int j=0;j<numEffects;j++)
			{
				if(one.getEffects().get(i).equals(two.getEffects().get(j)))
					sharedEffects.add(one.getEffect(i));
			}
		}
		return sharedEffects;
	}
	
	public HashMap<Ingredient, String> getCommonIngredients(Ingredient ingredient, int numEffects)
	{
		HashMap<Ingredient, String> commonIngredients=new HashMap<Ingredient, String>();
		
		for(int i=0;i<ingredients.size();i++)
		{
			if(!ingredients.get(i).equals(ingredient))
			{
				ArrayList<String> sharedEffects=sharedEffects(ingredient,ingredients.get(i),numEffects);
				for(int j=0;j<sharedEffects.size();j++)
				{
					commonIngredients.put(ingredients.get(i), sharedEffects.get(j));
				}
			}
		}
		
		return commonIngredients;
	}
	
	public ArrayList<Ingredient> getIngredientsWithEffect(String effect, int numEffects)
	{
		ArrayList<Ingredient> effectIngredients=new ArrayList<Ingredient>();
		
		for(int i=0;i<ingredients.size();i++)
		{
			for(int j=0;j<numEffects;j++)
			{
				if(ingredients.get(i).getEffect(j).equalsIgnoreCase(effect))
					effectIngredients.add(ingredients.get(i));
			}
		}
		
		return effectIngredients;
	}
	
	public boolean isValidEffect(String effect)
	{
		for(int i=0;i<effects.size();i++)
		{
			if(effects.get(i).equalsIgnoreCase(effect))
				return true;
		}
		return false;
	}
	
	public static void main(String[] args) {

	}
}